// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33637,y:32524,varname:node_3138,prsc:2|emission-7241-RGB,clip-8965-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32571,y:32775,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1074149,c2:0.9779412,c3:0.03595369,c4:1;n:type:ShaderForge.SFN_TexCoord,id:842,x:32450,y:33208,varname:node_842,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:7273,x:32414,y:33033,ptovrint:False,ptlb:Health,ptin:_Health,varname:_Health,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:4,max:4;n:type:ShaderForge.SFN_ComponentMask,id:5896,x:32628,y:33226,varname:node_5896,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-842-U;n:type:ShaderForge.SFN_ValueProperty,id:1550,x:32760,y:33429,ptovrint:False,ptlb:node_1550,ptin:_node_1550,varname:_node_1550,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:1896,x:33274,y:33303,ptovrint:False,ptlb:node_1896,ptin:_node_1896,varname:_node_1896,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Posterize,id:8965,x:33178,y:33071,varname:node_8965,prsc:2|IN-6614-OUT,STPS-1896-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:6614,x:32953,y:33244,varname:node_6614,prsc:2|IN-5896-OUT,IMIN-7833-OUT,IMAX-1550-OUT,OMIN-7833-OUT,OMAX-7273-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7833,x:32755,y:33309,ptovrint:False,ptlb:node_7833,ptin:_node_7833,varname:_node_7833,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:7241-7273-1896-1550-7833;pass:END;sub:END;*/

Shader "Shader Forge/hp_bar" {
    Properties {
        _Color ("Color", Color) = (0.1074149,0.9779412,0.03595369,1)
        _Health ("Health", Range(0.5, 4)) = 4
        _node_1896 ("node_1896", Float ) = 2
        _node_1550 ("node_1550", Float ) = 1
        _node_7833 ("node_7833", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Health;
            uniform float _node_1550;
            uniform float _node_1896;
            uniform float _node_7833;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                clip(floor((_node_7833 + ( (i.uv0.r.r - _node_7833) * (_Health - _node_7833) ) / (_node_1550 - _node_7833)) * _node_1896) / (_node_1896 - 1) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = _Color.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Health;
            uniform float _node_1550;
            uniform float _node_1896;
            uniform float _node_7833;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                clip(floor((_node_7833 + ( (i.uv0.r.r - _node_7833) * (_Health - _node_7833) ) / (_node_1550 - _node_7833)) * _node_1896) / (_node_1896 - 1) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
