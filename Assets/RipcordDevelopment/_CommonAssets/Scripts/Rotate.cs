﻿using UnityEngine;
using System.Collections;

// /-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\
//
// 						Ripcord Tools, Copyright © 2017, Ripcord Development
//												Rotate.cs
//												 v1.0.1
//										   info@ripcorddev.com
//
// \-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/

//ABOUT - Rotates an object on all three axes over time
//		  Negative values will rotate in the opposite direction
//		  Use the multiplier to modify all 3 values at once (1 - default speed)
//		  Object can be rotated using either Local (Self) or World coordinates

namespace Ripcord.Common {
	public class Rotate : MonoBehaviour {

		public Vector3 rotation = Vector3.zero;
		public float multiplier = 1.0f;
		public Space coordinateSpace = Space.World;


		void Update () {
			transform.Rotate(rotation * Time.deltaTime * multiplier, coordinateSpace);
		}
	}
}