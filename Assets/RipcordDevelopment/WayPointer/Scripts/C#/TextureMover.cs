﻿using UnityEngine;
using System.Collections;

// /-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\
//
// 						WayPointer 2.0, Copyright © 2017, Ripcord Development
//											 TextureMover.cs
//										   info@ripcorddev.com
//
// \-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/

//ABOUT - This script controls the offset behaviour of the texture on the marker mesh

public class TextureMover : MonoBehaviour {

	public enum MovementDirection {TowardsCenter, AwayFromCenter, PingPong}
	public MovementDirection movementDirection;									//The behaviour of the texture offset

	[Range (0.0f, 3.0f)]	public float movementSpeed = 1.0f;					//The speed at which the texture will move along the material's Y axis

	[Space (10)]
	[Range (0.0f, 0.5f)]	public float pingPongRange = 0.45f;					//The range of movement the texture will have in PingPong mode

	float pingPongOffset = 0.25f; 												//This value should remain at 0.25 as it accounts for the texture design...
																				//...allowing it to completely disappear at both the max and min of the pingPongRange

	void Start () {

		switch (movementDirection) {

		case MovementDirection.TowardsCenter:

			movementSpeed = Mathf.Abs(movementSpeed);
			break;

		case MovementDirection.AwayFromCenter:

			movementSpeed = -Mathf.Abs(movementSpeed);
			break;
		}
	}


	void Update () {

		if (movementDirection == MovementDirection.PingPong) {
			GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.0f, Mathf.Sin (Time.time * movementSpeed) * pingPongRange + pingPongOffset);
		}
		else {
			GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.0f, Time.time * movementSpeed);
		}
	}
}
