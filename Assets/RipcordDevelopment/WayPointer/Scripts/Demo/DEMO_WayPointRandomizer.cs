﻿using UnityEngine;
using System.Collections;
using Ripcord.Common;

// /-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\
//
// 						WayPointer 2.0, Copyright © 2017, Ripcord Development
//										DEMO_WayPointRandomizer.cs
//										   info@ripcorddev.com
//
// \-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/-\-/

//ABOUT - This script is intended for demonstration purposes only.  It is meant to show the thousands of combitions possible with WayPointer.

public class DEMO_WayPointRandomizer : MonoBehaviour {

	public GameObject[] markerMesh;					//The mesh object used for the main body of the marker
	public Material[] markerMaterial;				//The material to be applied to the marker mesh
	
	public GameObject[] markerDetailMesh;			//The mesh object used for the smaller objects that circle the marker mesh
	public Material[] markerDetailMaterial;			//The material to be applied to the marker detail
	
	public GameObject[] markerIconMesh;				//The mesh object used for the marker icon
	public Material[] markerIconMaterial;			//The material to be applied to the marker icon

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// GENERATE RANDOM COLOUR
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	public Color RandomColour () {

		Color randomColour = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f) );
		return randomColour;
	}

	void Update () {
	
		if (Input.GetKeyDown(KeyCode.Space)) {
			GenerateMarker();
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// GENERATE RANDOM MARKER
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	public void GenerateMarker () {
		
		Destroy(GameObject.Find("NewMarker") );																		//If there is a previous marker in the scene, delete it

		//GENERATE THE MARKER - - - - - - - - - - - - - - - - - - - -
		GameObject selectedMesh = markerMesh[Random.Range(0, markerMesh.Length)];									//Select a random marker mesh
		GameObject newMarker = (GameObject)Instantiate(selectedMesh, transform.position, transform.rotation);		//Spawn the new marker mesh in the scene
		newMarker.name = "NewMarker";																				//Rename the new marker mesh
		
		newMarker.GetComponent<Renderer>().material = markerMaterial[Random.Range(0, markerMaterial.Length)];		//Select a random material for the new marker
		newMarker.GetComponent<Renderer>().material.color = RandomColour();											//Set a random colour for the new marker

		TextureMover mover = newMarker.GetComponent<TextureMover>();												//Create a reference to the marker's TextureMover component
		mover.movementSpeed = Random.Range(-3.0f, 3.0f);															//Select a random movement speed for the marker
		float pingPongChance = Random.Range(0.0f, 9.0f);															//Select a random number
		if (pingPongChance >= 7) {																					//If the random number is less than the chance to PingPong...
			mover.movementDirection = TextureMover.MovementDirection.PingPong;										//...set the texture movement to PingPong (back and forth)
		}
		
		//GENERATE THE MARKER DETAIL - - - - - - - - - - - - - - - - - - - -
		GameObject selectedDetail = markerDetailMesh[Random.Range(0, markerDetailMesh.Length)];									//Select a random marker detail mesh
		GameObject newMarkerDetail = (GameObject)Instantiate(selectedDetail, transform.position, transform.rotation);			//Spawn the selected marker detail in the scene
		newMarkerDetail.name = "Marker.Detail";																					//Set the name for the new marker detail
		newMarkerDetail.transform.parent = newMarker.transform;																	//Make the new detail a child of the new marker object
		
		newMarkerDetail.GetComponent<Renderer>().material = markerDetailMaterial[Random.Range(0, markerDetailMaterial.Length)];	//Select a random material for the new marker detail
		newMarkerDetail.GetComponent<Renderer>().material.color = RandomColour();												//Set a random colour for the new marker detail

		Rotate rotator = newMarkerDetail.AddComponent<Rotate>();																//Add a Rotate component to the new marker detail
		rotator.rotation = new Vector3(0.0f, Random.Range(-180.0f, 180.0f), 0.0f);												//Set a random rotation for the Rotate component
		
		//GENERATE THE MARKER ICON - - - - - - - - - - - - - - - - - - - -
		GameObject selectedIcon = markerIconMesh[Random.Range(0, markerIconMesh.Length)];										//Select a random marker icon mesh
		GameObject newMarkerIcon = (GameObject)Instantiate(selectedIcon, transform.position, transform.rotation);				//Spawn the selected marker icon in the scene
		newMarkerIcon.name = "Marker.Icon";																						//Set the name for the new marker icon
		newMarkerIcon.transform.parent = newMarker.transform;																	//Make the new icon a child of the new marker object
		newMarkerIcon.transform.position += new Vector3(0.0f, 0.01f, 0.0f);														//Offset the height of the new icon

		newMarkerIcon.GetComponent<Renderer>().material = markerIconMaterial[Random.Range(0, markerIconMaterial.Length)];		//Select a random materail for the new marker icon
		newMarkerIcon.GetComponent<Renderer>().material.color = RandomColour();													//Set a random colour for the new marker icon
		float scaleVariation = Random.Range(0.75f, 1.25f);																		//Select a random scale value for the new marker icon
		newMarkerIcon.transform.localScale = new Vector3(scaleVariation, 1.0f, scaleVariation);									//Set the new marker icon's scale value
	}
}