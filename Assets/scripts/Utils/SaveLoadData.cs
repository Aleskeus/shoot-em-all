﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoadData  {

    public static void Save(string filename, System.Object content)
    {
        BinaryFormatter bf = new BinaryFormatter();
        SurrogateSelector ss = new SurrogateSelector();
        Vector2IntSerializationSurrogate v2iss = new Vector2IntSerializationSurrogate();
        ss.AddSurrogate(typeof(Vector2Int),
                        new StreamingContext(StreamingContextStates.All),
                        v2iss);

        bf.SurrogateSelector = ss;
        FileMode current_file_mode = (File.Exists(Application.streamingAssetsPath + filename)) ? FileMode.Open : FileMode.Create;
        FileStream file = File.Open(Application.streamingAssetsPath + filename, current_file_mode);

        bf.Serialize(file, content);
        file.Close();

        


    }

    public static System.Object Load(string filename)
    {
        if (File.Exists(Application.streamingAssetsPath + filename))
        {
            Debug.Log("file exists");
            BinaryFormatter bf = new BinaryFormatter();
            SurrogateSelector ss = new SurrogateSelector();
            Vector2IntSerializationSurrogate v2iss = new Vector2IntSerializationSurrogate();
            ss.AddSurrogate(typeof(Vector2Int),
                            new StreamingContext(StreamingContextStates.All),
                            v2iss);

            bf.SurrogateSelector = ss;
            FileStream file = File.Open(Application.streamingAssetsPath + filename, FileMode.Open);
            System.Object data = (System.Object)bf.Deserialize(file);
            file.Close();
            return data;
        }
        return null;
    }
}
