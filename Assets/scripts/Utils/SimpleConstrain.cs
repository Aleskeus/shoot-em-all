﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleConstrain : MonoBehaviour {

	public GameObject targetObject;

	void Update () {
        transform.position = targetObject.transform.position;
        transform.rotation = targetObject.transform.rotation;
	}
}
