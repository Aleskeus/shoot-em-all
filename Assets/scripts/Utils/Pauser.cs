﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Input;

public class Pauser : MonoBehaviour {

    [SerializeField]
    private GameObject pausePanel;
    [SerializeField]
    private BaseMenu pauseMenuGenerator;
    PlayerController player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }
    void Update()
    {
        if (InputManager.IsJustPressed("Pause"))
        {
            if (!pausePanel.activeInHierarchy)
            {
                PauseGame();
            }
            else
            {
                ContinueGame();
            }
        }
    }
    private void PauseGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        pauseMenuGenerator.enabled = true;
        player.StopCoroutine("Shoot");
    }
    public void ContinueGame()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        pauseMenuGenerator.enabled = false;
        player.StartCoroutine("Shoot");
    }
}
