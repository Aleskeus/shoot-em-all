﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PooledObject
{

    public Vector3 speed;
    private MeshRenderer meshRenderer;
    public GameObject splintersPrefab;
    GameObject splinters;

    void Start () {
        meshRenderer = GetComponent<MeshRenderer>();
    }
	
	void Update () {
        transform.position += speed * Time.deltaTime;

        if (meshRenderer != null && meshRenderer.isVisible == false)
        {
            GameObjectsPooler.objectPool.RemovePooledObject(gameObject);
        }
    }


    public void RemoveProjectile()
    {
        splinters = GameObjectsPooler.objectPool.GetPooledObject(splintersPrefab);
        splinters.transform.position = transform.position;
        splinters.transform.eulerAngles = new Vector3(transform.eulerAngles.x, (transform.eulerAngles.y - 90), 90);
        splinters.SetActive(true);
        GameObjectsPooler.objectPool.RemovePooledObject(gameObject);
        
    }
}
