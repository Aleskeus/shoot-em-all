﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : PooledObject
{
    public GameObject explosion;
    public float delayBeforeExplosion;
    public float explosionDuration;

    void OnEnable () {
        StartCoroutine(DestroyInSec());
    }
	
	void Update () {
		
	}

    IEnumerator DestroyInSec()
    {
        yield return new WaitForSeconds(delayBeforeExplosion);
        explosion.SetActive(true);
        explosion.GetComponent<Collider>().enabled = true;
        yield return new WaitForSeconds(explosionDuration);
        explosion.SetActive(false);
        GameObjectsPooler.objectPool.RemovePooledObject(gameObject);
    }
}
