﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splinters : PooledObject {

    public ParticleSystem particleSystemObject;

    void OnEnable () {
        particleSystemObject.Play();
        StartCoroutine(DestroyInSec());
    }
	
	void Update () {
		
	}

    IEnumerator DestroyInSec()
    {
        yield return new WaitForSeconds(2f);
        GameObjectsPooler.objectPool.RemovePooledObject(gameObject);
    }
}
