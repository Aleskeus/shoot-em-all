using UnityEngine;
using System.Collections;
using com.ootii.Input;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.AI;

public class GameControl : MonoBehaviour
{

	public static GameControl control;
    [HideInInspector] public float aspect_ratio;
    
    //[Range(1, 5)]
    //public int level_to_start;
    [SerializeField] int active_level = 1;
    public StartingCoordinatesFormat player_spawn_points;
    public GameObject input_prefab;
    GameObject input;
    [SerializeField]
    public Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> global_craters;
    public int global_size = 10000;
    public const int chunk_size = 70;
    [Range(0, 10)]
    public int craters_density;
    [Range(0, 50)]
    public int craters_radius;
    [Range(0, 50)]
    public int craters_depth;
    public string craters_filename;
	public int IsLevelTest { get; set; }
    [SerializeField]
    private float health = 1000f;
    [SerializeField]
    private float max_health = 1000f;

    public int Active_level
    {
        get
        {
            return active_level;
        }
    }
  

	public float Health {
		get {
			return health;
		}
	}
    public float Health_for_UI
    {
        get
        {
            return Mathf.Clamp(health/max_health, 0, 1);
        }
    }
	
    public void addHealth(float amount)
    {
        health += amount;
        
    }

    public void remove_health(float amount)
    {
        health -= amount;
        if (health <= 0) Fail_level();
    }
 
    public void Continue_game()
    {
        SceneManager.LoadScene("Map", LoadSceneMode.Single);
        SceneManager.LoadScene("Level_" + active_level.ToString(), LoadSceneMode.Additive);
        resetState();
    }

    public void Start_new_game()
    {
        active_level = 1;
        SceneManager.LoadScene("Map", LoadSceneMode.Single);
        SceneManager.LoadScene("Level_" + active_level.ToString(), LoadSceneMode.Additive);
        resetState();
    }
    public void Play_next_level()
    {
        active_level++;
        SceneManager.LoadScene("Map", LoadSceneMode.Single);
        SceneManager.LoadScene("Level_" + active_level.ToString(), LoadSceneMode.Additive);
        resetState();
    }

    public void Fail_level()
    {
        SceneManager.LoadScene("End_level_fail", LoadSceneMode.Single);
    }

    public void Level_win()
    {
        SceneManager.LoadScene("End_level_won", LoadSceneMode.Single);
    }

    public void resetState()
	{
		health = max_health;
        Time.timeScale = 1;
    }

	void Awake ()
	{
		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
            Generate_global_craters();
            if (SceneManager.GetActiveScene().name == "Map")
            {
                resetState();
            }
        } else if (control != this) {
			Destroy (gameObject);
        }
        aspect_ratio = Screen.width / (float)Screen.height;
    }

    public void Generate_global_craters()
    {
        global_craters = (Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>>)SaveLoadData.Load(craters_filename);
        if (global_craters == null)
        {
            Debug.Log("creating new craters file");
            global_craters = CratersGenerator.GenerateCraters(global_size, chunk_size, craters_density, craters_radius, craters_depth);
            SaveLoadData.Save(craters_filename, global_craters);
        }
    }


}
