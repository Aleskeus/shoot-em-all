﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelInitialization : MonoBehaviour {

    [HideInInspector]
    public Transform player;
    public Camera _camera;
    public StartingCoordinatesFormat playerSpawnPoints;

    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        NavMeshAgent nav = player.gameObject.GetComponent<NavMeshAgent>();
        nav.enabled = false;
        player.position = new Vector3(playerSpawnPoints.startingCoordinates[GameControl.control.Active_level - 1].x, 1.2f, playerSpawnPoints.startingCoordinates[GameControl.control.Active_level - 1].y);
        _camera.transform.position += player.position;
        nav.enabled = true;
    }
}
