﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTargets : MonoBehaviour {

    public GameObject pointerPrefab;
    GameObject pointer;
    public List<GameObject> targets;
    int i = 0;

    public GameObject arrowPrefab;
    GameObject arrow;
    public int distanceFromPlayer;
    Transform player;

    void Start () {
        pointer = Instantiate(pointerPrefab);
        pointer.transform.position = targets[0].transform.position;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        arrow = Instantiate(arrowPrefab);
        arrow.transform.position = player.position + (pointer.transform.position - player.position).normalized;
    }
	 
	void Update () {
        if (targets[i].activeInHierarchy != false)
        {
            pointer.transform.position = targets[i].transform.position;
        }
        else
        {
            if (++i < targets.Count)
            {
                if (targets[i].activeInHierarchy != false)
                {
                    pointer.transform.position = targets[i].transform.position;
                }
            }
            else GameControl.control.Level_win();
        }
        arrow.transform.position = player.position + (pointer.transform.position - player.position).normalized * distanceFromPlayer;
        arrow.transform.rotation = Quaternion.LookRotation(player.position - pointer.transform.position);
    }
}
