﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(EndlessTerrain))]
public class EndlessTerrainEditor : Editor {
    public override void OnInspectorGUI()
    {
        EndlessTerrain mapGen = (EndlessTerrain)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Generate"))
        {
            mapGen.GenerateGlobalCraters();
        }
        
    }
}
