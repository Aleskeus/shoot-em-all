﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HardWorker {
    public static Queue<Thread_info> dataQueue = new Queue<Thread_info>();

    static HardWorker()
    {
        ThreadPool.SetMaxThreads(Environment.ProcessorCount, Environment.ProcessorCount * 10);

    }
    public static void DoWork(Action<object> hardFunc)
    {

        ThreadPool.QueueUserWorkItem(new WaitCallback(hardFunc));
    }

    public static void DoWork(Func<object> hardFunc, Action<object> callback)
    {

        ThreadPool.QueueUserWorkItem(new WaitCallback(someObj => DataThread(hardFunc, callback)));
    }

    static void DataThread(Func<object> generateData, Action<object> callback)
    {
        object data = generateData();
        lock (dataQueue)
        {
            dataQueue.Enqueue(new Thread_info(callback, data));
        }
    }

    public struct Thread_info
    {
        public readonly Action<object> callback;
        public readonly object parameter;

        public Thread_info(Action<object> callback, object parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }

    }

}
