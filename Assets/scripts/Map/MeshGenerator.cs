﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator
{
    public static MeshData GeneratePlaneForTexture(int sizeX, int sizeY)
    {
        MeshData plane = new MeshData(2, 2);
        plane.vertices[0] = new Vector3(0, 0, 0);
        plane.vertices[1] = new Vector3(sizeX, 0, 0);
        plane.vertices[2] = new Vector3(sizeX, 0, sizeY);
        plane.vertices[3] = new Vector3(0, 0, sizeY);

        plane.AddTriangle(0, 2, 1);
        plane.AddTriangle(2, 0, 3);

        plane.uvs[0] = new Vector2(0, 0);
        plane.uvs[1] = new Vector2(1, 0);
        plane.uvs[2] = new Vector2(1, 1);
        plane.uvs[3] = new Vector2(0, 1);
        return plane;
    }

    public static MeshData GenerateHillsMesh(float[,] heightMap, float heightMultiplier, AnimationCurve heightCurve)
    {
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);
        float topLeftX = (width - 1) / -2f;
        float topLeftZ = (height - 1) / 2f;
        

        MeshData meshData = new MeshData(width, height);
        int vertexIndex = 0;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                meshData.vertices[vertexIndex] = new Vector3(topLeftX + x, heightCurve.Evaluate(heightMap[x, y]) * heightMultiplier, topLeftZ - y);
                meshData.uvs[vertexIndex] = new Vector2(x / (float)width, y / (float)height);

                if (x < width - 1 && y < height - 1)
                {
                    meshData.AddTriangle(vertexIndex, vertexIndex + width + 1, vertexIndex + width);
                    meshData.AddTriangle(vertexIndex + width + 1, vertexIndex, vertexIndex + 1);
                }
                vertexIndex++;
            }
        }
        return meshData;
    }

    public static MeshData GenerateCubeMesh(float[,] heightMap, float heightMultiplier, AnimationCurve _heightCurve)
    { return GenerateCubeMesh(heightMap, heightMultiplier, _heightCurve, 1); }

    public static MeshData GenerateCubeMesh(float[,] heightMap)
    { return GenerateCubeMesh(heightMap, 1, null, 1); }

    //lod - пока просто заглушка, возможно будет реализовано в будущем
    public static MeshData GenerateCubeMesh(float[,] heightMap, float heightMultiplier, AnimationCurve _heightCurve, int lod)
    {
        AnimationCurve heightCurve = null;
        System.Random rnd = new System.Random();
        if (_heightCurve != null)
        { 
            heightCurve = new AnimationCurve(_heightCurve.keys);
        }
        
        int width = heightMap.GetLength(0) - 1;
        int height = heightMap.GetLength(1) - 1;

        MeshData meshData = new MeshData(width, height, MeshData.MeshTypes.Cubes);
        int vertexIndex = 0;
        float uvSize = 4;

        //горизонтальные квадратики регулярной структуры
        for (int y = 0; y < height; y += lod)
        {
            for (int x = 0; x < width; x += lod)
            {
                float heightMapValue = EvaluateHeight(heightMap[x, y], heightMultiplier, heightCurve);
                meshData.vertices[vertexIndex] = new Vector3(x - 0.5f, heightMapValue, y - 0.5f);
                meshData.vertices[vertexIndex + 1] = new Vector3(x + 0.5f, heightMapValue, y - 0.5f);
                meshData.vertices[vertexIndex + 2] = new Vector3(x + 0.5f, heightMapValue, y + 0.5f);
                meshData.vertices[vertexIndex + 3] = new Vector3(x - 0.5f, heightMapValue, y + 0.5f);

                
                int xUV = rnd.Next((int)uvSize);
                int yUV = rnd.Next((int)uvSize);

                meshData.uvs[vertexIndex] = new Vector2(xUV / uvSize, yUV / uvSize);
                meshData.uvs[vertexIndex + 1] = new Vector2((xUV +1)/ uvSize, yUV / uvSize);
                meshData.uvs[vertexIndex + 2] = new Vector2((xUV + 1) / uvSize, (yUV + 1) / uvSize);
                meshData.uvs[vertexIndex + 3] = new Vector2(xUV / uvSize, (yUV + 1) / uvSize);

                meshData.AddTriangle(vertexIndex, vertexIndex + 2, vertexIndex + 1);
                meshData.AddTriangle(vertexIndex + 3, vertexIndex + 2, vertexIndex + 0);

                vertexIndex += 4;
            }
        }
        //вертикальные квадратики регулярной структуры в плоскости yz
        for (int y = 0; y < height; y += lod)
        {
            for (int x = 0; x < width; x += lod)
            {
                float heightMapValue = EvaluateHeight(heightMap[x, y], heightMultiplier, heightCurve);
                float heightMapNextPoint = EvaluateHeight(heightMap[x+1, y], heightMultiplier, heightCurve);
                
                meshData.vertices[vertexIndex] = new Vector3(x + 0.5f, heightMapValue, y + 0.5f);
                meshData.vertices[vertexIndex + 1] = new Vector3(x + 0.5f, heightMapValue, y - 0.5f);
                meshData.vertices[vertexIndex + 2] = new Vector3(x + 0.5f, heightMapNextPoint, y - 0.5f);
                meshData.vertices[vertexIndex + 3] = new Vector3(x + 0.5f, heightMapNextPoint, y + 0.5f);

                int x_UV = rnd.Next((int)uvSize);
                int y_UV = rnd.Next((int)uvSize);

                meshData.uvs[vertexIndex] = new Vector2(x_UV / uvSize, y_UV / uvSize);
                meshData.uvs[vertexIndex + 1] = new Vector2((x_UV + 1) / uvSize, y_UV / uvSize);
                meshData.uvs[vertexIndex + 2] = new Vector2((x_UV + 1) / uvSize, (y_UV + 1) / uvSize);
                meshData.uvs[vertexIndex + 3] = new Vector2(x_UV / uvSize, (y_UV + 1) / uvSize);

                meshData.AddTriangle(vertexIndex, vertexIndex + 2, vertexIndex + 1);
                meshData.AddTriangle(vertexIndex, vertexIndex + 3, vertexIndex + 2);
                vertexIndex += 4;
            }
        }

        for (int y = 0; y < height; y += lod)
        {
            for (int x = 0; x < width; x += lod)
            {
                float heightMapValue = EvaluateHeight(heightMap[x, y], heightMultiplier, heightCurve);
                float heightMapNextPoint = EvaluateHeight(heightMap[x, y + 1], heightMultiplier, heightCurve);

                meshData.vertices[vertexIndex] = new Vector3(x - 0.5f, heightMapValue, y + 0.5f);
                meshData.vertices[vertexIndex + 1] = new Vector3(x + 0.5f, heightMapValue, y + 0.5f);
                meshData.vertices[vertexIndex + 2] = new Vector3(x + 0.5f, heightMapNextPoint, y + 0.5f);
                meshData.vertices[vertexIndex + 3] = new Vector3(x - 0.5f, heightMapNextPoint, y + 0.5f);

                int xUV = rnd.Next((int)uvSize);
                int yUV = rnd.Next((int)uvSize);

                meshData.uvs[vertexIndex] = new Vector2(xUV / uvSize, yUV / uvSize);
                meshData.uvs[vertexIndex + 1] = new Vector2((xUV + 1) / uvSize, yUV / uvSize);
                meshData.uvs[vertexIndex + 2] = new Vector2((xUV + 1) / uvSize, (yUV + 1) / uvSize);
                meshData.uvs[vertexIndex + 3] = new Vector2(xUV / uvSize, (yUV + 1) / uvSize);

                meshData.AddTriangle(vertexIndex, vertexIndex + 2, vertexIndex + 1);
                meshData.AddTriangle(vertexIndex, vertexIndex + 3, vertexIndex + 2);
                vertexIndex += 4;
            }
        }
        
        return meshData;
    }
    private static float EvaluateHeight(float height, float heightMultiplier, AnimationCurve heightCurve)
    {
        if (heightCurve != null)
        {
            return heightCurve.Evaluate(height) * heightMultiplier;
        }
        else
        {
            return height;
        }
    }

    public static MeshData GenerateVenus(float[,] heightMap, float heightMultiplier, AnimationCurve heightCurve, float angle)
    {

        MeshData mesh_data = GenerateCubeMesh(heightMap, heightMultiplier, heightCurve);
        mesh_data.vertices = FlatToSphere(mesh_data.vertices, angle);
        return mesh_data;
    }

    private static Vector3[] FlatToSphere(Vector3[] verts, float angle) {
        for(int i = 0; i < verts.Length; i++)
        {
            Vector3 new3dCoords = Vector3.zero;
           
            float angleX = Convert.ToSingle(2 * Math.PI * verts[i].x * 0.01f * angle);
            float angleZ = Convert.ToSingle((Math.PI /2 + Math.PI * verts[i].z * 0.01f) * angle);

            new3dCoords.x = Convert.ToSingle(verts[i].y * Math.Sin(angleX) * Math.Cos(angleZ));
            new3dCoords.y = Convert.ToSingle(verts[i].y * Math.Cos(angleX) * Math.Cos(angleZ));
            new3dCoords.z = Convert.ToSingle(verts[i].y * Math.Sin(angleZ ));
            verts[i] = new3dCoords;
        }
        return verts;
    }
}

public class MeshData
{
    public enum MeshTypes{plane_for_texture, Cubes}
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;

    int triangleIndex;

    public MeshData(int meshWidth, int meshHeight)
    {
        vertices = new Vector3[meshWidth * meshHeight];
        uvs = new Vector2[meshWidth * meshHeight];
        triangles = new int[(meshWidth - 1) * (meshHeight - 1) * 6];
    }

    public MeshData(int meshWidth, int meshHeight, MeshTypes type)
    {
        vertices = new Vector3[meshWidth * meshHeight * 12];
        uvs = new Vector2[meshWidth * meshHeight * 12];
        triangles = new int[(3 * meshWidth * meshHeight) * 6]; 
    }

    public void AddTriangle(int a, int b, int c)
    {
        triangles[triangleIndex] = a;
        triangles[triangleIndex + 1] = b;
        triangles[triangleIndex + 2] = c;
        triangleIndex += 3;
    }

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        return mesh;
    }

}
