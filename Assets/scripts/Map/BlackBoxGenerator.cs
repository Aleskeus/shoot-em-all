﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BlackBoxGenerator : MonoBehaviour {
    public GameObject referenceObject;
    public GameObject cratersPoolParent;
    public List<CraterBlackBox> cratersPool = new List<CraterBlackBox>();
    public int xOffsetFromCenter;
    public int yOffsetFromCenter;

    [SerializeField]
    public Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> globalCraters;
    public string cratersFilename;
    Vector2Int currentCenterChunk;
    HashSet<Vector2Int> activeKeys = new HashSet<Vector2Int>();
    Dictionary<Vector2Int, Crater> craters = new Dictionary<Vector2Int, Crater>();

    
    public void GenerateBlackBox()
    {
        GenerateGlobalCraters();
        ClearCratersPool();
        GenerateCratersPool();
        currentCenterChunk = new Vector2Int((int)Math.Floor(referenceObject.transform.position.x / EndlessTerrain.chunkSize),
                                        (int)Math.Floor(referenceObject.transform.position.z / EndlessTerrain.chunkSize));
        ActivateColliders(currentCenterChunk);
    }

    void GenerateGlobalCraters()
    {
        if (globalCraters == null)
        {
            print("craters start loading");
            globalCraters = (Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>>)SaveLoadData.Load(cratersFilename);
            print("craters end loading");
        }
    }

    void ActivateColliders(Vector2Int center)
    {
        activeKeys.Clear();
        for (int y_offset = center.y - yOffsetFromCenter; y_offset <= center.y + yOffsetFromCenter; y_offset++)
        {
            for (int x_offset = center.x - xOffsetFromCenter; x_offset <= center.x + xOffsetFromCenter; x_offset++)
            {
                activeKeys.Add(new Vector2Int(x_offset, y_offset));

            }
        }
        craters.Clear();
        foreach (Vector2Int key in activeKeys)
        {
            if (globalCraters.ContainsKey(key))
            {
                Dictionary<Vector2Int, Crater> local_craters = globalCraters[key];

                foreach (Vector2Int local_key in globalCraters[key].Keys)
                {
                    if (!craters.ContainsKey(local_key))
                    {
                        craters.Add(local_key, local_craters[local_key]);
                    }
                }
            }
        }

        foreach (KeyValuePair<Vector2Int, Crater> kvp in craters)
        {
            bool crater_assigned = false;
            for (int i = 0; i < cratersPool.Count; ++i)
            {
                if (!cratersPool[i].isUsed)
                {
                    cratersPool[i].obj.transform.position = new Vector3(kvp.Key.x, 0, kvp.Key.y);
                    cratersPool[i].obj.transform.localScale = Vector3.one * kvp.Value.radius * 2;
                    cratersPool[i].obj.SetActive(true);
                    cratersPool[i].isUsed = true;
                    crater_assigned = true;
                    break;
                }
            }
            if (!crater_assigned)
            {
                Debug.LogWarning("not all craters was initialized");
            }
        }
    }

    public void GenerateCratersPool()
    {
        foreach (Transform child in cratersPoolParent.transform)
        {
            cratersPool.Add(new CraterBlackBox(child.gameObject));
        }
    }

    public void ClearCratersPool()
    {
        foreach(CraterBlackBox crater in cratersPool)
        {
            crater.obj.SetActive(false);
        }
        cratersPool.Clear();
    }
}


public class CraterBlackBox
{
    public bool isUsed;
    public GameObject obj;

    public CraterBlackBox(GameObject _obj)
    {
        isUsed = false;
        obj = _obj;
    }
}