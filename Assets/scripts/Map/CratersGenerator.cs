﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class CratersGenerator {

   
    public static Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> GenerateCraters(int globalSize, int chunkSize, int density, int averageRadius, int averageDepth)
    {
        Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> globalCraters = new Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>>();

        int cratersQuantity = (int)density * globalSize * globalSize / chunkSize / chunkSize;
        HashSet<Vector2Int> globalCratersKeys = new HashSet<Vector2Int>();
        for (int i = 0; i <= cratersQuantity; i++)
        {
            globalCratersKeys.Clear();
            int x = (int)UnityEngine.Random.Range(-globalSize, globalSize);
            int y = (int)UnityEngine.Random.Range(-globalSize, globalSize);
            int radius = (int)UnityEngine.Random.Range(averageRadius * 0.5f, averageRadius * 1.5f);
            int depth = (UnityEngine.Random.Range(0, 1.5f) > 1) ?  (int)UnityEngine.Random.Range(averageDepth * 1.4f, averageDepth * 2f) : -(int)UnityEngine.Random.Range(averageDepth * 0.2f, averageDepth * 1.5f);
            Vector2Int hill_center = GetRandomPointInCircle(radius);
            int step_from_center = UnityEngine.Random.Range(6, 11);
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)x / chunkSize), (int)Math.Floor((double)y / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)(x + radius + 1) / chunkSize), (int)Math.Floor((double)y / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)(x + radius + 1) / chunkSize), (int)Math.Floor((double)(y+radius + 1) / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)x / chunkSize), (int)Math.Floor((double)(y + radius + 1) / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)(x - radius - 1) / chunkSize), (int)Math.Floor((double)(y + radius + 1) / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)(x - radius - 1) / chunkSize), (int)Math.Floor((double)y / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)(x - radius - 1) / chunkSize), (int)Math.Floor((double)(y - radius - 1) / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)x / chunkSize), (int)Math.Floor((double)(y - radius - 1) / chunkSize)));
            globalCratersKeys.Add(new Vector2Int((int)Math.Floor((double)(x + radius+ 1) / chunkSize), (int)Math.Floor((double)(y - radius - 1) / chunkSize)));
            foreach (Vector2Int global_craters_key in globalCratersKeys)
            {
                if (globalCraters.ContainsKey(global_craters_key))
                {
                    Dictionary<Vector2Int, Crater> craters = globalCraters[global_craters_key];
                    if (!craters.ContainsKey(new Vector2Int(x, y)))
                    {
                        craters.Add(new Vector2Int(x, y), new Crater(radius, depth, hill_center, step_from_center));
                    }
                }
                else
                {
                    Dictionary<Vector2Int, Crater> craters = new Dictionary<Vector2Int, Crater>();
                    craters.Add(new Vector2Int(x, y), new Crater(radius, depth, hill_center, step_from_center));
                    globalCraters.Add(global_craters_key, craters);
                }
            }
        }
          
        return globalCraters;
    }

    static Vector2Int GetRandomPointInCircle(int maxRadius)
    {
        int radius = (int)UnityEngine.Random.Range(0, maxRadius);
        float angle = UnityEngine.Random.Range(0, 2*Mathf.PI);
        return new Vector2Int((int)(Mathf.Cos(angle) * radius), (int)(Mathf.Sin(angle) * radius));
    }
}

[Serializable]
public struct Crater
{
    public int radius;
    public int depth;
    public Vector2Int center;
    public int stepFromCenter;

    public Crater(int radius, int depth, Vector2Int center, int stepFromCenter)
    {
        this.radius = radius;
        this.depth = depth;
        this.center = center;
        this.stepFromCenter = stepFromCenter;
    }
}
