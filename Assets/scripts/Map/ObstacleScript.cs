﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : PooledObject
{

    void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.tag == "Player_bullet" || otherCollider.tag == "Enemy_bullet")
        {
            otherCollider.gameObject.GetComponent<Projectile>().RemoveProjectile();
        }
    }
}
