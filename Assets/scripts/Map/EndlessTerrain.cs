﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using com.ootii.Messages;

public class EndlessTerrain : MonoBehaviour
{
    public Action<object> actionToWork;


    public Transform viewer;
    Vector2Int currentCenterChunk;
    Vector2Int oldCenterChank;
    public Material mapMaterial;

    Vector2 viewerPositionOld;
    public int globalSize = 10000;
    public const int chunkSize = 70;
    [Range(0, 10)]
    public int cratersDensity;
    [Range(0, 50)]
    public int cratersRadius;
    [Range(0, 50)]
    public int cratersDepth;
    int poolSize = 80;
    public Vector2Int visibleChunks;

    HashSet<Vector2Int> currentChunkCoords = new HashSet<Vector2Int>();
    HashSet<Vector2Int> prevChunkCoords = new HashSet<Vector2Int>();
    HashSet<Vector2Int> newChunkCoords = new HashSet<Vector2Int>();
    HashSet<Vector2Int> deactivatingChunkCoords = new HashSet<Vector2Int>();

    Dictionary<Vector2, LandscapeChunk> activeTerrainChunks = new Dictionary<Vector2, LandscapeChunk>();
    List<LandscapeChunk> pooledTerrainChunks = new List<LandscapeChunk>();
    [SerializeField]
    public Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> globalCraters;
    public string cratersFilename;


    void Start()
    {
        globalCraters = GameControl.control.global_craters;
        CreateTerrainChunksPool();
        viewer = GameObject.FindGameObjectWithTag("Player").transform;
        UpdateVisibleChunks();
        MessageDispatcher.SendMessage(this, "Update_map", currentCenterChunk, 0);

    }

    void CreateTerrainChunksPool()
    {
        for (int i = 0; i < poolSize; i++)
        {
            pooledTerrainChunks.Add(new LandscapeChunk(transform, mapMaterial));
        }
    }

    void Update()
    {
        currentCenterChunk = new Vector2Int((int)Math.Floor(viewer.position.x / chunkSize),
                                              (int)Math.Floor(viewer.position.z / chunkSize));
        if (currentCenterChunk != oldCenterChank)
        {
            MessageDispatcher.SendMessage(this, "Update_map", currentCenterChunk, 0);
           UpdateVisibleChunks();
            oldCenterChank = currentCenterChunk;
        }

        if (HardWorker.dataQueue.Count > 0)
        {
            for (int i = 0; i < HardWorker.dataQueue.Count; i++)
            {
                HardWorker.Thread_info threadInfo = HardWorker.dataQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }

    void UpdateVisibleChunks()
    {
        currentChunkCoords.Clear();
        for (int y_offset = -visibleChunks.y; y_offset <= visibleChunks.y + 1; y_offset++)
        {
            for (int x_offset = -visibleChunks.x; x_offset <= visibleChunks.x + 1; x_offset++)
            {
                currentChunkCoords.Add(new Vector2Int(currentCenterChunk.x + x_offset,
                                                        currentCenterChunk.y + y_offset));
            }
        }
        newChunkCoords.Clear();
        newChunkCoords.UnionWith(currentChunkCoords);
        newChunkCoords.ExceptWith(prevChunkCoords);
        foreach (Vector2Int chunkPlace in newChunkCoords)
        {
            for (int i = 0; i < pooledTerrainChunks.Count; ++i)
            {
                if (!pooledTerrainChunks[i].IsActive())
                {
                    if(globalCraters.ContainsKey(chunkPlace))
                    {
                        pooledTerrainChunks[i].Activate(chunkPlace, chunkSize, globalCraters[chunkPlace]);
                    }
                    else
                    {
                        pooledTerrainChunks[i].Activate(chunkPlace, chunkSize, null);
                    }
                    
                    activeTerrainChunks.Add(chunkPlace, pooledTerrainChunks[i]);
                    break;
                }
            }
        }

        deactivatingChunkCoords.Clear();
        deactivatingChunkCoords.UnionWith(prevChunkCoords);
        deactivatingChunkCoords.ExceptWith(currentChunkCoords);
        foreach (Vector2Int chunk_place in deactivatingChunkCoords)
        {
            activeTerrainChunks[chunk_place].Deactivate();
            activeTerrainChunks.Remove(chunk_place);
        }

        prevChunkCoords.Clear();
        prevChunkCoords.UnionWith(currentChunkCoords);
    }

    public void GenerateGlobalCraters()
    {
        globalCraters = (Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>>)SaveLoadData.Load(cratersFilename);
        if (globalCraters == null)
        {
            Debug.Log("creating new craters file");
            globalCraters = CratersGenerator.GenerateCraters(globalSize, chunkSize, cratersDensity, cratersRadius, cratersDepth);
            SaveLoadData.Save(cratersFilename, globalCraters);
        }
    }
}