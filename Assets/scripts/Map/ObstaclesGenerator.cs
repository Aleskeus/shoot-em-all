﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class ObstaclesGenerator : MonoBehaviour {

    public EndlessTerrain endlessTerrainScript;
    public Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> globalCraters;
    public Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> tempGlobalCraters = new Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>>();
    public GameObject hillCollider;
    

    HashSet<Vector2Int> activeKeys = new HashSet<Vector2Int>();
    Dictionary<Vector2Int, Crater> craters = new Dictionary<Vector2Int, Crater>();
    Dictionary<Vector2Int, Crater> tempDict;
    int xOffsetFromCenter;
    int yOffsetFromCenter;

    void Awake()
    {

         MessageDispatcher.AddListener("Update_map", RecalculateColliders, true);
        xOffsetFromCenter = endlessTerrainScript.visibleChunks.x;
        yOffsetFromCenter = endlessTerrainScript.visibleChunks.y;
    }

    void Start()
    {
        globalCraters = GameControl.control.global_craters;
        foreach (KeyValuePair<Vector2Int, Dictionary<Vector2Int, Crater>> local_dicts in globalCraters)
        {
            tempDict = new Dictionary<Vector2Int, Crater>();
            foreach (KeyValuePair<Vector2Int, Crater> kvp in local_dicts.Value)
            {
                if (kvp.Value.depth > 0)
                {
                    tempDict.Add(kvp.Key, kvp.Value);
                }
            }
            tempGlobalCraters.Add(local_dicts.Key, tempDict);
        } 
        globalCraters = tempGlobalCraters;
    }
    void OnDisable()
    {
         MessageDispatcher.RemoveListener("Update_map", RecalculateColliders);
    }
   
    void RecalculateColliders(IMessage rMessage)
    {
        GameObjectsPooler.objectPool.SetInactiveAll(hillCollider);
        ActivateColliders((Vector2Int)rMessage.Data);
       
    }

    
    void ActivateColliders(Vector2Int center)
    {
        activeKeys.Clear();
        for (int yOffset = center.y - yOffsetFromCenter; yOffset <= center.y + yOffsetFromCenter; yOffset++)
        {
            for (int xOffset = center.x - xOffsetFromCenter; xOffset <= center.x + xOffsetFromCenter; xOffset++)
            {
                activeKeys.Add(new Vector2Int(xOffset, yOffset));

            }
        }

        craters.Clear();
        foreach (Vector2Int key in activeKeys)
        {
            if (globalCraters.ContainsKey(key))
            {
                Dictionary<Vector2Int, Crater> localCraters = globalCraters[key];
                
                foreach (Vector2Int localKey in globalCraters[key].Keys)
                {
                    if (!craters.ContainsKey(localKey))
                    {
                        
                        craters.Add(localKey, localCraters[localKey]);
                    }
                }
            }
        }

        foreach (KeyValuePair<Vector2Int, Crater> kvp in craters)
        {
            CapsuleCollider sc = GameObjectsPooler.objectPool.GetPooledObject(hillCollider).GetComponent<CapsuleCollider>();

            sc.gameObject.SetActive(true);
            sc.transform.position = new Vector3(kvp.Key.x, 0, kvp.Key.y);
            sc.radius = kvp.Value.radius;
            //Debug.Log("SphereCollider " + sc.gameObject.activeSelf + "  " + sc.gameObject.activeInHierarchy + ", " + sc.transform.position + ", " + sc.radius);
        }
    }
}
