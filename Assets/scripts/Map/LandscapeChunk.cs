﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class LandscapeChunk
{
    [Range(0, 10)]
    public int cratersDensity;
    [Range(0, 50)]
    public int cratersRadius;
    [Range(0, 50)]
    public int cratersDepth;
    public System.Object someObj;

    GameObject meshObject;
    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    MeshData meshData;
    Vector2 position;

    public bool auto_update;
    

    public LandscapeChunk(Transform parent, Material material)
    {
        meshObject = new GameObject("Terrain Chunk");
        meshRenderer = meshObject.AddComponent<MeshRenderer>();
        meshFilter = meshObject.AddComponent<MeshFilter>();
        meshRenderer.material = material;
        meshObject.transform.parent = parent;
        Deactivate();
    }
    

    public void Activate(Vector2Int center, int size, Dictionary<Vector2Int, Crater> craters)
    {
        position = center * size;
        Vector3 positionV3 = new Vector3(position.x, 0, position.y);
        meshObject.transform.position = positionV3;

        HardWorker.DoWork(() => MeshDataGeneration(center, size, craters), ActivatePart);
        meshObject.SetActive(true);
    }

    MeshData MeshDataGeneration(Vector2Int center, int size, Dictionary<Vector2Int, Crater> craters)
    {
        float[,] height_map = GenerateHeightMap(size, center * size, craters);
        meshData = MeshGenerator.GenerateCubeMesh(height_map);
        return meshData;
    }

    public void ActivatePart(object meshData)
    {
        meshFilter.mesh = ((MeshData)meshData).CreateMesh();
    }

    public void Deactivate()
    {
        meshObject.SetActive(false);
    }

    public bool IsActive()
    {
        return meshObject.activeInHierarchy;
    }

    float[,] GenerateHeightMap(int size, Vector2Int center, Dictionary<Vector2Int, Crater> craters)
    {
        float[,] height_map = new float[size + 1, size + 1];

        for (int y = 0; y <= size; y++)
        {
            for (int x = 0; x <= size; x++)
            {
                height_map[x, y] = 0;
                if (craters != null)
                {
                    foreach (Vector2Int coord in craters.Keys)
                    {
                        if ((x + center.x - coord.x) * (x + center.x - coord.x) + (y + center.y - coord.y) * (y + center.y - coord.y) < craters[coord].radius * craters[coord].radius)
                        {
                            if (height_map[x, y] == 0)
                            {
                                if (craters[coord].depth < 0)
                                {
                                    height_map[x, y] = craters[coord].depth;
                                }
                                else
                                {
                                    height_map[x, y] = craters[coord].depth * GetHeight(x + center.x, y + center.y, coord, craters[coord]);
                                }
                            }
                            else if (craters[coord].depth > 0)
                            {
                                if (craters[coord].depth * GetHeight(x + center.x, y + center.y, coord, craters[coord]) > height_map[x, y])
                                {
                                    height_map[x, y] = craters[coord].depth * GetHeight(x + center.x, y + center.y, coord, craters[coord]);
                                }
                            }
                            else if (craters[coord].depth < 0)
                            {
                                if (height_map[x, y] < 0)
                                {
                                    if (craters[coord].depth < height_map[x, y])
                                    {
                                        height_map[x, y] = craters[coord].depth;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return height_map;
    }

    float GetHeight(int x, int y, Vector2Int coord, Crater crater)
    {
        Vector2Int knap = coord + crater.center;
        float distanceFromKnap = (knap - new Vector2Int(x, y)).magnitude;
        return (1 - distanceFromKnap * crater.stepFromCenter / crater.radius / 2 / 10);
    }
}

