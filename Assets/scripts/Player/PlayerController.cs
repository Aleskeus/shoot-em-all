﻿using com.ootii.Input;
using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed = 1;
    public float bulletSpeed = 250;

    public GameObject projectilePrefab;
    public Transform Gun;
    public Transform spawnPoint;
    float horMovement;
    float vertMovement;
    float carCurrentOrientation;
    float gunCurrentOrientation;
    float currentDeltaOrientation;

    Vector3 currentDeltaPosition = Vector3.zero;
    float horDirection;
    float vertDirection;
    bool isShooting;
    public float shootDelay = 0.1f;

    void Start()
    {
        StartCoroutine("Shoot");
    }
    void FixedUpdate()
    {
        horMovement = InputManager.MovementX;
        vertMovement = InputManager.MovementY;
        Vector2 movement = Vector2.ClampMagnitude(new Vector2(horMovement, vertMovement), 1);
        horDirection = InputManager.ViewX;
        vertDirection = InputManager.ViewY;
        if (horMovement != 0.0f || vertMovement != 0.0f)
        {
            Move(movement.x, movement.y);
            Rotate(horMovement, vertMovement);
        }
        if (horDirection != 0.0f || vertDirection != 0.0f)
        {
            gunCurrentOrientation = Get_angle(horDirection, vertDirection);
            currentDeltaOrientation = carCurrentOrientation - gunCurrentOrientation;
        }
        else
        {
            gunCurrentOrientation = carCurrentOrientation - currentDeltaOrientation;
        }
        Gun.eulerAngles = new Vector3(0, 90 -gunCurrentOrientation, 0);
    }

    private void Rotate(float h, float v)
    {
        carCurrentOrientation = Get_angle(h, v);
        transform.eulerAngles = new Vector3(0, -carCurrentOrientation, 0); //- - ибо поворот в юнити не в ту сторону, что в математике
    }
     
    void Move(float h, float v)
    {
        currentDeltaPosition = new Vector3(h, 0, v) * speed;
        transform.position += currentDeltaPosition;
    }

    void OnTriggerEnter(Collider otherCollider)
    {
        if(otherCollider.tag == "Enemy_bullet")
        {
            GameControl.control.remove_health(1);
            otherCollider.gameObject.GetComponent<Projectile>().RemoveProjectile();
        }

        if (otherCollider.tag == "Enemy_explosion")
        {
            GameControl.control.remove_health(3);
            otherCollider.gameObject.GetComponent<SphereCollider>().enabled = false;
        }
    }


    public IEnumerator Shoot()
    {
        while (true)
        {
            if (InputManager.IsPressed("Fire3"))
            {
                GameObject shot = GameObjectsPooler.objectPool.GetPooledObject(projectilePrefab);

                shot.transform.position = spawnPoint.transform.position;
                shot.transform.eulerAngles = new Vector3(0, 90 -gunCurrentOrientation, 0);
                shot.SetActive(true);
                Projectile shotScript = shot.GetComponent<Projectile>();

                shotScript.speed = new Vector3(Mathf.Cos(gunCurrentOrientation * Mathf.Deg2Rad), 0, Mathf.Sin(gunCurrentOrientation * Mathf.Deg2Rad)) * bulletSpeed;
                yield return new WaitForSeconds(shootDelay);
            }
            else
            {
                yield return null;
            }
        }
    }
    private float Get_angle(float x, float y)
    {
        return Mathf.Atan2(y, x) * Mathf.Rad2Deg;
    }
}
