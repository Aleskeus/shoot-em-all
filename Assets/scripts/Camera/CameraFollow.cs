﻿using UnityEngine;
using System.Collections;
using com.ootii.Input;

public class CameraFollow : MonoBehaviour
{
    public Transform target;            
    public float smoothing = 5f;        
    public float viewOffsetAmpltude = 5f;

    Vector3 offset;                     
    Vector3 viewOffset;
    float horDirection;
    float vertDirection;

    void Awake ()
    {
        offset = transform.position - target.position;
    }


    void FixedUpdate ()
    {
        
        horDirection = InputManager.ViewX;
        vertDirection = InputManager.ViewY;
        viewOffset = new Vector3(horDirection, 0, vertDirection) * viewOffsetAmpltude;
        Vector3 targetCamPos = target.position + offset + viewOffset;
        transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}
