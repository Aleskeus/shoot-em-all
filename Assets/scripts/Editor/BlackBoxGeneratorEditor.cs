﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BlackBoxGenerator))]
public class BlackBoxGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BlackBoxGenerator bbGen = (BlackBoxGenerator)target;
        if (DrawDefaultInspector())
        {
            bbGen.GenerateBlackBox();
        }
        if (GUILayout.Button("Generate"))
        {
            bbGen.GenerateBlackBox();
        }
    }
}
