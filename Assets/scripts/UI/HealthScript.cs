﻿
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour {

    public Image healthBar;

    void Start()
    {
        healthBar = GetComponent<Image>();
    }
    void Update()
    {
        healthBar.fillAmount = 1 - GameControl.control.Health_for_UI;
    }
}
