﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Menu_strctr", menuName = "menu_asset", order = 1)]
public class MenuStructureFormat : ScriptableObject {

    public string menuName;
    public List<MenuPanel> menuPanels;
}
