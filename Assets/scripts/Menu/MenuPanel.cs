﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MenuPanel
{
    public string panelName;
    public List<string> menuItems;
}
