﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelWonMenu : BaseMenu {

    public MenuStructureFormat endGameStructure;
    public int LastLevel = 1;

    public override void Awake () {
        if(GameControl.control.Active_level == 1)
        {
            structure = endGameStructure;
        }
        base.Awake();
	}

}
