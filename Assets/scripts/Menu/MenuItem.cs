﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class MenuItem : MonoBehaviour {

    public string text;
    public int indexInPanel;
    [SerializeField]
    private bool isActive;
    public bool IsActive
    {
        get
        {
            return isActive;
        }
        set
        {
            if (value)
                transform.localScale = new Vector3(1.4f, 1.4f, 1);
            else
                transform.localScale = Vector3.one;
            isActive = value;
        }
    }

    void Start () {
        transform.GetComponentInChildren<Text>().text = text;
    }
}
