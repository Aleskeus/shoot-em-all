﻿using com.ootii.Input;
using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class BaseMenu : MonoBehaviour {

    public GameObject rootCanvas;
    public MenuStructureFormat structure;
    public GameObject panelPrefab;
    public GameObject itemPrefab;

    List<GameObject> panels = new List<GameObject>();
    MenuItem[][] panelsAndItems;

    panelItem activeItem = new panelItem(0, 0);
    panelItem prevActiveItem = new panelItem(0, 0);

    public virtual void Awake()
    {
        GenerateMenu();
        panels[activeItem.panel].SetActive(false);
    }

    void OnEnable () {
        
        MessageDispatcher.AddListener("_MoveVertical", EnumInputMessageType.INPUT_JUST_PRESSED, ChangeActiveItem);
        MessageDispatcher.AddListener("Menu_up", EnumInputMessageType.INPUT_JUST_PRESSED, PrevActiveItem);
        MessageDispatcher.AddListener("Menu_down", EnumInputMessageType.INPUT_JUST_PRESSED, NextActiveItem);
        MessageDispatcher.AddListener("Menu_prev", EnumInputMessageType.INPUT_JUST_PRESSED, PrevPanel);
        MessageDispatcher.AddListener("Menu_next", EnumInputMessageType.INPUT_JUST_PRESSED, NextPanel);
        panels[activeItem.panel].SetActive(true);
    }
    void OnDisable()
    {
        MessageDispatcher.RemoveListener("_MoveVertical", EnumInputMessageType.INPUT_JUST_PRESSED, ChangeActiveItem);
        MessageDispatcher.RemoveListener("Menu_up", EnumInputMessageType.INPUT_JUST_PRESSED, PrevActiveItem);
        MessageDispatcher.RemoveListener("Menu_down", EnumInputMessageType.INPUT_JUST_PRESSED, NextActiveItem);
        MessageDispatcher.RemoveListener("Menu_prev", EnumInputMessageType.INPUT_JUST_PRESSED, PrevPanel);
        MessageDispatcher.RemoveListener("Menu_next", EnumInputMessageType.INPUT_JUST_PRESSED, NextPanel);
        panels[activeItem.panel].SetActive(false);
    }

    public void GenerateMenu()
    {
        panelsAndItems = new MenuItem[structure.menuPanels.Count][];
        for (int i = 0; i < structure.menuPanels.Count; i++)
        {
            panelsAndItems[i] = new MenuItem[structure.menuPanels[i].menuItems.Count];
            GeneratePanel(i);
        }
        panels[0].SetActive(true);
        panelsAndItems[0][0].IsActive = true;
    }

    public void GeneratePanel(int panelNumber)
    {
        panels.Add(Instantiate(panelPrefab));
        panels[panelNumber].transform.SetParent(rootCanvas.transform);
        panels[panelNumber].transform.position = Vector3.zero;
        panels[panelNumber].transform.localScale = Vector3.one;
        RectTransform rt = panels[panelNumber].GetComponent<RectTransform>();
        rt.anchoredPosition3D = Vector3.zero;
        panels[panelNumber].name = structure.menuPanels[panelNumber].panelName;
        panels[panelNumber].SetActive(false);
        //Debug.Log(panels[panelNumber].name + " strange coordinates " + panels[panelNumber].transform.position + ", rect" + rt.anchoredPosition3D);
        for (int i = 0; i < structure.menuPanels[panelNumber].menuItems.Count; i++)
        {
            panelsAndItems[panelNumber][i] = Instantiate(itemPrefab).transform.GetComponent<MenuItem>();
            panelsAndItems[panelNumber][i].transform.SetParent(panels[panelNumber].transform);
            panelsAndItems[panelNumber][i].transform.localPosition = Vector3.zero + new Vector3(110, -i * 50f, 0);
            panels[panelNumber].transform.localScale = Vector3.one;
            panelsAndItems[panelNumber][i].name = structure.menuPanels[panelNumber].menuItems[i];
            panelsAndItems[panelNumber][i].indexInPanel = i;
            panelsAndItems[panelNumber][i].text = structure.menuPanels[panelNumber].menuItems[i];
            panelsAndItems[panelNumber][i].IsActive = true;
            panelsAndItems[panelNumber][i].IsActive = false;
            EventTrigger trigger = panelsAndItems[panelNumber][i].gameObject.AddComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((eventData) => { ChangeActiveItem(trigger.gameObject.GetComponent<MenuItem>().indexInPanel - panelsAndItems[activeItem.panel][activeItem.item].indexInPanel); });
            trigger.triggers.Add(entry);
            EventTrigger.Entry entry2 = new EventTrigger.Entry();
            entry2.eventID = EventTriggerType.PointerClick;
            entry2.callback.AddListener((eventData) => { NextPanel(); });
            trigger.triggers.Add(entry2);
        }

    }
   
    public void ChangeItem(int old, int n)
    {
        print("old active " + old + ", new active " + n );
    }
    public void PrevActiveItem(IMessage rMessage)
    {
        ChangeActiveItem(-1);
    }
    public void NextActiveItem(IMessage rMessage)
    {
        ChangeActiveItem(1);
    }
    public void ChangeActiveItem(IMessage rMessage)
    {
        int deltaActiveItem = ((float)rMessage.Data > 0) ? -1 : 1;
        ChangeActiveItem(deltaActiveItem);
    }
    public void ChangeActiveItem(int deltaAciveItem)
    {
        if (deltaAciveItem == 0) return;
        prevActiveItem = activeItem;
        if(activeItem.item+deltaAciveItem >= 0)
            activeItem.item = (activeItem.item + deltaAciveItem) % structure.menuPanels[activeItem.panel].menuItems.Count;
        else
            activeItem.item = (activeItem.item + deltaAciveItem) % structure.menuPanels[activeItem.panel].menuItems.Count
                + structure.menuPanels[activeItem.panel].menuItems.Count;
        if (panelsAndItems[activeItem.panel][activeItem.item] != null)
        {
            panelsAndItems[activeItem.panel][activeItem.item].IsActive = true;
            panelsAndItems[prevActiveItem.panel][prevActiveItem.item].IsActive = false;
        }
    }

    public void NextPanel(IMessage rMessage)
    {
        if (panelsAndItems[activeItem.panel][activeItem.item] != null)
        {
            NextPanel();
        }
    }
    public void NextPanel()
    {

        prevActiveItem = activeItem;
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Continue")
        {
            GameControl.control.Continue_game();
        }
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Start new game")
        {
            GameControl.control.Start_new_game();
        }
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Play again")
        {
            GameControl.control.Continue_game();
        }
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Next")
        {
            GameControl.control.Play_next_level();
        }
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Quit game")
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Resume")
        {
            FindObjectOfType<Pauser>().ContinueGame();
            return;
        }
        if (structure.menuPanels[activeItem.panel].menuItems[activeItem.item] == "Main menu")
        {
            SceneManager.LoadScene("Main_menu", LoadSceneMode.Single);
        }
        for (int i = 0; i < structure.menuPanels.Count; i++)
        {
            if (structure.menuPanels[i].panelName == structure.menuPanels[activeItem.panel].menuItems[activeItem.item])
            {
                activeItem.panel = i;
                activeItem.item = 0;
            }
        }
        panelsAndItems[prevActiveItem.panel][prevActiveItem.item].IsActive = false;
        panels[prevActiveItem.panel].SetActive(false);
        panels[activeItem.panel].SetActive(true);
        panelsAndItems[activeItem.panel][activeItem.item].IsActive = true;
    }

    public void PrevPanel(IMessage rMessage)
    {
        prevActiveItem = activeItem;
        for (int i = 0; i < structure.menuPanels.Count; i++)
        {
            for (int j = 0; j < structure.menuPanels[i].menuItems.Count; j++)
            {
                if (structure.menuPanels[i].menuItems[j] == structure.menuPanels[activeItem.panel].panelName)
                {
                    activeItem.panel = i;
                    activeItem.item = j;
                }
            }
        }
        panelsAndItems[prevActiveItem.panel][prevActiveItem.item].IsActive = false;
        panels[prevActiveItem.panel].SetActive(false);
        panels[activeItem.panel].SetActive(true);
        panelsAndItems[activeItem.panel][activeItem.item].IsActive = true;
    }

}

public struct panelItem
{
    public int panel, item;

    public panelItem(int _panel, int _item)
    {
        panel = _panel;
        item = _item;
    }
}