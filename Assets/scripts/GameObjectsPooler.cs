﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameObjectsPooler : MonoBehaviour
{

    public static GameObjectsPooler objectPool;
    [SerializeField]
    private int pooledAmount = 20;
    public int totalPooled = 0;
    Transform navMeshGenerator;
    public GameObject craterCollider;
    [SerializeField]
    List<GameObject> pooledObjectList;
    Dictionary<int, GameObject> pooledObjectIDs;
    private Dictionary<int, List<GameObject>> variousPooledObjects;

    void Awake()
    {
        try
        {
            navMeshGenerator = FindObjectOfType<NavMeshGenerator>().gameObject.transform;
        }
        catch
        {
            navMeshGenerator = transform;
        }
        objectPool = this;

        pooledObjectIDs = new Dictionary<int, GameObject>();
        variousPooledObjects = new Dictionary<int, List<GameObject>>();
        for (int i = 0; i < pooledObjectList.Count; i++)
        {
            pooledObjectIDs.Add(i, pooledObjectList[i]);
            List<GameObject> _pool = new List<GameObject>();
            variousPooledObjects.Add(i, _pool);
            for (int j = 0; j < pooledAmount; ++j)
            {
                GameObject _obj = (GameObject)Instantiate(pooledObjectList[i]);
                totalPooled++;
                _obj.transform.parent = transform;
                if (i == 0)
                {
                    _obj.transform.parent = navMeshGenerator;
                }
                _obj.SetActive(false);
                _pool.Add(_obj);
            }
        }
    }

    public void SetInactiveAll(GameObject _prefab)
    {
        List<GameObject> _pool = variousPooledObjects[_prefab.GetComponent<PooledObject>().poolID];
        foreach (GameObject _obj in _pool)
        {
            if (_obj.activeInHierarchy)
            {
                _obj.SetActive(false);
            }
        }
   }

    public GameObject GetPooledObject(GameObject _prefab)
    {
        GameObject _obj = null;
        //Debug.Log("Get_pooled_object name " + _prefab.name +" key is: " + _prefab.GetInstanceID());
        List <GameObject> _pool = variousPooledObjects[_prefab.GetComponent<PooledObject>().poolID];
        for (int i = 0; i < _pool.Count; i++) {
            if (!_pool.ElementAt(i).activeInHierarchy)
            {
                _obj = _pool.ElementAt(i);
                return _obj;
            }
        }
            
        _obj = (GameObject)Instantiate(pooledObjectIDs[_prefab.GetComponent<PooledObject>().poolID]);
        totalPooled++;
        _obj.SetActive(false);
        _obj.transform.parent = transform;
        if (_prefab.GetComponent<PooledObject>().poolID == 0)
        {
            _obj.transform.parent = navMeshGenerator;
        }
        _pool.Add(_obj);
        return _obj;
    }

    public void RemovePooledObject(GameObject _prefab)
    {
        //Debug.Log(_obj.name);
        _prefab.SetActive(false);
    }
}
