﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour {

    public Transform player;
    public GameObject explosionPrefab;
    protected GameObject explosionInstance;
    public float triggerDistance;
    public float delayBeforeExplosion;
    bool activated = false;
    Animator ac;

    public virtual void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        ac = GetComponent<Animator>();
    }

    public virtual void Update()
    {
        if ((transform.position - player.position).magnitude < triggerDistance)
        {
            if (!activated)
            {
                ac.SetTrigger("Explode");
                StartCoroutine("Explode");
                activated = true;
            }
        }
    }

    IEnumerator Explode()
    {
        yield return new WaitForSeconds(delayBeforeExplosion);
        explosionInstance = GameObjectsPooler.objectPool.GetPooledObject(explosionPrefab);
        explosionInstance.transform.position = transform.position;
        explosionInstance.SetActive(true);
        Destroy(gameObject);
    }
}
