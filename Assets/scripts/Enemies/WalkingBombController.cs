﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingBombController : BombController
{

    UnityEngine.AI.NavMeshAgent nav;            
    public float health;
    public int viewDistance;

    public override void Start()
    {
        base.Start();
        
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        nav.enabled = true;
    }

    void OnTriggerEnter(Collider otherCollider)
    {
        //Debug.Log("other col tag is: " + other_collider.tag);
        if (otherCollider.tag == "Player_bullet")
        {
            otherCollider.gameObject.GetComponent<Projectile>().RemoveProjectile();
            health -= 1;
            if (health <= 0)
            {
                explosionInstance = GameObjectsPooler.objectPool.GetPooledObject(explosionPrefab);
                explosionInstance.transform.position = transform.position;
                explosionInstance.SetActive(true);
                Destroy(gameObject);
            }
        }
    }

    public override void Update()
    {
        if ((transform.position - player.position).magnitude < viewDistance)
        {
            if ((transform.position - player.position).magnitude > triggerDistance & nav.enabled)
            {

                nav.SetDestination(player.position);
            }
            else
            {
                nav.enabled = false;
            }
        }
        base.Update();
    }
}
