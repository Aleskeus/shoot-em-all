﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingEnemyController : EnemyController
{
    public float triggerDistance = 40;
    public int deltaToPlayer = 10;
    public int viewDistance;
    public GameObject tankTower;
    UnityEngine.AI.NavMeshAgent nav;

    public override void Start () {
        if(tankTower == null)
        {
            tankTower = gameObject;
        }
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        base.Start();
    }

    void Update()
    {
        

        if ((transform.position - player.position).magnitude < viewDistance)
        {
            if (!lockPosition)
            {
                nav.isStopped = false;
                if ((transform.position - player.position).magnitude > triggerDistance)
                {
                    Vector3 delta = (transform.position - player.position).normalized * deltaToPlayer * 1.2f;
                    delta += new Vector3(Random.Range(0, deltaToPlayer), 0, Random.Range(0, deltaToPlayer));
                    nav.SetDestination(player.position + delta);
                }
            }else
            {
                nav.isStopped = true;
            }

            if (!lockRotation)
            {
                Vector3 dir = new Vector3(Vector3.MoveTowards(transform.position, player.transform.position, 1f).x - transform.position.x,
                        0,
                        Vector3.MoveTowards(transform.position, player.transform.position, 1f).z - transform.position.z).normalized;
                tankTower.transform.eulerAngles = new Vector3(0, (Mathf.LerpAngle(tankTower.transform.eulerAngles.y, 90 - Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg, 0.3f)), 0);
            }
        }

        if (health <= 0)
        {
            nav.isStopped = true;
        }
    }
   
}
