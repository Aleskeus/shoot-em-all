﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemiesManager))]
public class EnemiesManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        EnemiesManager em = (EnemiesManager)target;
        DrawDefaultInspector();
        if (GUILayout.Button("Save_current"))
        {
            em.SaveCurrentChildren();
        }

        if (GUILayout.Button("Load"))
        {
            em.LoadEnemiesLocationFromFile();
        }

        if (GUILayout.Button("Delete_all_children"))
        {
            em.DeleteAllChild();
        }
    }
}
