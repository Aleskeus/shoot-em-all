﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingTower : EnemyController
{

    public Transform tracker;
    public float trackingDistance = 150;

	
	// Update is called once per frame
	void Update () {
        if ((transform.position - player.transform.position).magnitude < trackingDistance)
        {
            Vector3 dir = new Vector3(Vector3.MoveTowards(transform.position, player.transform.position, 1f).x - transform.position.x,
                                    0,
                                    Vector3.MoveTowards(transform.position, player.transform.position, 1f).z - transform.position.z).normalized;
            tracker.transform.eulerAngles = new Vector3(0, (Mathf.LerpAngle(tracker.transform.eulerAngles.y, -90 - Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg, 0.3f)), 0);

        }
    }
}
