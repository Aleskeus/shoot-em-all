﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Starting_coordinates", menuName = "Starting_coordinates", order = 1)]
public class StartingCoordinatesFormat : ScriptableObject
{
    public List<Vector2Int> startingCoordinates;
}
