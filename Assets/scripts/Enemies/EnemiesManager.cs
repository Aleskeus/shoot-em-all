﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class EnemiesManager : MonoBehaviour {

    public EnemiesList enemiesList;
    public string enemiesFilename;
    [Range(1, 5)]
    public int levelToStart;
    public bool loadEnemies;
    Dictionary<Vector2Int, int> enemiesToSave = new Dictionary<Vector2Int, int>();

    void Start () {
        foreach (Transform enemy in gameObject.transform)
        {
            Destroy(enemy.gameObject);
        }
        if (loadEnemies)
        {
            LoadEnemiesLocationFromFile();
        }
    }
	
    public void SaveCurrentChildren()
    {
        enemiesToSave.Clear();
#if UNITY_EDITOR
        foreach (Transform enemy in gameObject.transform)
        {
            UnityEngine.Object prefab = PrefabUtility.GetPrefabParent(enemy.gameObject);
            for (int i = 0; i < enemiesList.enemiesList.Count; i++)
            {
                if (prefab.name == enemiesList.enemiesList[i].name)
                {
                    if (!enemiesToSave.ContainsKey(new Vector2Int((int)enemy.position.x, (int)enemy.position.z))) {
                        enemiesToSave.Add(new Vector2Int((int)enemy.position.x, (int)enemy.position.z), i);
                        break;
                    }
                }
            }
        }
        if (enemiesToSave.Count > 0)
        {
            SaveLoadData.Save(enemiesFilename + levelToStart.ToString(), enemiesToSave);
        }
#endif
    }

    public void LoadEnemiesLocationFromFile()
    {
        if (Application.isPlaying)
        {
            levelToStart = GameControl.control.Active_level;
        }
        enemiesToSave = (Dictionary<Vector2Int, int>)SaveLoadData.Load(enemiesFilename + levelToStart.ToString());

        foreach (KeyValuePair<Vector2Int, int> positionIndexInList in enemiesToSave)
        {
            GameObject enemy = null;
            if (Application.isPlaying)
            {
                enemy = Instantiate(enemiesList.enemiesList[positionIndexInList.Value]);
            }
            else
            {
#if UNITY_EDITOR
                enemy = (GameObject)PrefabUtility.InstantiatePrefab(enemiesList.enemiesList[positionIndexInList.Value]);
#endif
            }
            enemy.transform.parent = transform;
            enemy.transform.position = new Vector3Int(positionIndexInList.Key.x, 2, positionIndexInList.Key.y);
        }
    }

    public void DeleteAllChild()
    {
#if UNITY_EDITOR
        foreach (Transform e in transform)
        {
            print(e.name);
            DestroyImmediate(e.gameObject);
        }
#endif
    }
}
