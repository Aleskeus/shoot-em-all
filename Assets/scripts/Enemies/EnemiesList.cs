﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemies", menuName = "enemies_list", order = 1)]
public class EnemiesList : ScriptableObject
{
    public List<GameObject> enemiesList;
}
