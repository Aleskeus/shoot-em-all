﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour {

    public GameObject player;
    public Transform tracker;
    public float laserDistance = 150;
    public float trackingDistance = 150;
    public float laserDelay = 1f;
    public float damagePerUpdate= 0f;

    LineRenderer line;

    void Start () {
        player = GameObject.FindGameObjectsWithTag("Player")[0];
        line = GetComponent<LineRenderer>();
        line.enabled = false;
    }

    void Update () {
        if ((transform.position - player.transform.position).magnitude < trackingDistance)
        {
            Vector3 dir = new Vector3(Vector3.MoveTowards(transform.position, player.transform.position, 1f).x - transform.position.x,
                                    0,
                                    Vector3.MoveTowards(transform.position, player.transform.position, 1f).z - transform.position.z).normalized;
            tracker.transform.eulerAngles = new Vector3(0, (Mathf.LerpAngle(tracker.transform.eulerAngles.y, 90 - Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg, 0.3f)), 0);

        }
        if ((transform.position - player.transform.position).magnitude < laserDistance)
        {
            line.SetPosition(0, transform.position);
            line.SetPosition(1, player.transform.position);
            line.enabled = true;
            GameControl.control.remove_health(damagePerUpdate);
        }
        else
        {
            line.enabled = false;
        }
    }

}
