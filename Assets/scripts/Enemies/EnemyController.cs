﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyController : MonoBehaviour
{

    bool shooting;
    public Transform player;
    public GameObject bullet;
    public GameObject DeathExplosion;
    public GameObject aliveMesh;
    public GameObject deathMesh;
    public float health;
    public float shootsDelay = 1f;
    public float stringOfBurstsDelay = 3f;
    public int bulletsInBurst = 5;
    public float shotgunScatter;
    public float bulletsScatter;
    public float bulletSpeed;
    public float shootingDistance = 100;
    public Transform[] bulletSpawnPoints;
    protected bool lockRotation = false;
    protected bool lockPosition = false;

    public virtual void Start()
    {
        if (bulletSpawnPoints[0] == null) bulletSpawnPoints[0] = this.transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        StartCoroutine("Shoot");
    }

    void OnTriggerEnter(Collider otherCollider)
    {
        //Debug.Log("other col tag is: " + other_collider.tag);
        if (otherCollider.tag == "Player_bullet")
        {
            otherCollider.gameObject.GetComponent<Projectile>().RemoveProjectile();
            health -= 1;
            if (health <= 0)
            {
                aliveMesh.SetActive(false);
                deathMesh.SetActive(true);
                DeathExplosion.SetActive(true);
                StopCoroutine("Shoot");
                
            }
        }
    }


    IEnumerator Shoot()
    {
        while (true)
        {
            if ((transform.position - player.position).magnitude < shootingDistance) {
                Vector3 dir = new Vector3(Vector3.MoveTowards(transform.position, player.transform.position, 1f).x - transform.position.x,
                        0,
                        Vector3.MoveTowards(transform.position, player.transform.position, 1f).z - transform.position.z).normalized;
                dir = Quaternion.Euler(0, - bulletsInBurst * shotgunScatter / 2f, 0) * dir;
                lockRotation = true;
                lockPosition = true;
                int bullet_spawn_points_counter = 0;
                for (int i = 0; i < bulletsInBurst; i++)
                {
                    GameObject shot = GameObjectsPooler.objectPool.GetPooledObject(bullet);

                    int spawnPointIndex = bullet_spawn_points_counter % bulletSpawnPoints.Length;
                    bullet_spawn_points_counter++;
                    shot.transform.position = bulletSpawnPoints[spawnPointIndex].position;
                    shot.SetActive(true);
                    Projectile projectile = shot.GetComponent<Projectile>();
                    dir = Quaternion.Euler(0, shotgunScatter, 0) * dir;
                    Vector3 final_dir = Quaternion.Euler(0, Random.Range(-bulletsScatter, bulletsScatter), 0) * dir;
                    shot.transform.eulerAngles = new Vector3(0,90 -Mathf.Atan2(final_dir.z, final_dir.x) * Mathf.Rad2Deg, 0);
                    projectile.speed = final_dir * bulletSpeed;
                    yield return new WaitForSeconds(shootsDelay);
                }
                lockRotation = false;
                lockPosition = false;
                yield return new WaitForSeconds(stringOfBurstsDelay);
            }
            else
            {
                yield return null;
            }
        }
    }
}


