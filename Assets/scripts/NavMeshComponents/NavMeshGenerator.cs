﻿using System.Collections;
using System.Collections.Generic;
using com.ootii.Collections;
using UnityEngine;
using UnityEngine.AI;
using com.ootii.Messages;
using System.Linq;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class NavMeshGenerator : MonoBehaviour {

    public Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>> globalCraters;
    public BoxCollider mainLand;
    public GameObject craterCollider;
    public NavMeshSurface surface;

    HashSet<Vector2Int> activeKeys = new HashSet<Vector2Int>();
    Dictionary<Vector2Int, Crater> allCraters = new Dictionary<Vector2Int, Crater>();
    Dictionary<Vector2Int, Crater> craters = new Dictionary<Vector2Int, Crater>();
    int xOffsetFromCenter;
    int yOffsetFromCenter;

    void Awake()
    {
        xOffsetFromCenter = (int)mainLand.size.x / EndlessTerrain.chunkSize / 2;
        yOffsetFromCenter = (int)mainLand.size.z / EndlessTerrain.chunkSize / 2;
    }

    void Start () {
        globalCraters = GameControl.control.global_craters;
    }
    
    void RecalculateNavmesh(IMessage rMessage)
    {
        mainLand.transform.position = new Vector3(((Vector2Int)rMessage.Data).x * EndlessTerrain.chunkSize, -5f, ((Vector2Int)rMessage.Data).y * EndlessTerrain.chunkSize);
        ActivateColliders((Vector2Int)rMessage.Data);
        surface.BuildNavMesh();
       GameObjectsPooler.objectPool.SetInactiveAll(craterCollider);
    }


    public void ActivateCollidersFromEditor()
    {
#if UNITY_EDITOR
        globalCraters = (Dictionary<Vector2Int, Dictionary<Vector2Int, Crater>>)SaveLoadData.Load("/craters");
        foreach (KeyValuePair<Vector2Int, Dictionary<Vector2Int, Crater>> kvp in globalCraters)
        {
            Dictionary<Vector2Int, Crater> localCraters = kvp.Value;
            foreach (KeyValuePair<Vector2Int, Crater> posCrater in localCraters)
            {
                if (!allCraters.ContainsKey(posCrater.Key))
                {
                    allCraters.Add(posCrater.Key, posCrater.Value);
                }
            }
        }

        foreach(KeyValuePair<Vector2Int, Crater> posCrater in allCraters)
        {
            GameObject crater = PrefabUtility.InstantiatePrefab(craterCollider) as GameObject;
            crater.transform.parent = gameObject.transform;
            SphereCollider sc = crater.GetComponent<SphereCollider>();
            sc.transform.position = new Vector3(posCrater.Key.x, 0, posCrater.Key.y);
            sc.radius = posCrater.Value.radius + 1;
        }
#endif
    }

    public void DestroyChildSphereColliders()
    {
        foreach (Transform sc in gameObject.transform)
        {
            if(sc.gameObject.GetComponent<SphereCollider>() != null)
            {
                DestroyImmediate(sc.gameObject);
            }
        }
    }
    void ActivateColliders(Vector2Int center)
    {
        activeKeys.Clear();
        for (int yOffset = center.y - yOffsetFromCenter; yOffset <= center.y + yOffsetFromCenter; yOffset++)
        {
            for (int xOffset = center.x - xOffsetFromCenter; xOffset <= center.x + xOffsetFromCenter; xOffset++)
            {
                activeKeys.Add(new Vector2Int(xOffset, yOffset));

            }
        }

        craters.Clear();
        foreach (Vector2Int key in activeKeys)
        {
            if (globalCraters.ContainsKey(key))
            {
                Dictionary<Vector2Int, Crater> localCraters = globalCraters[key];

                foreach (Vector2Int localKey in globalCraters[key].Keys)
                {
                    if(!craters.ContainsKey(localKey))
                    {
                        craters.Add(localKey, localCraters[localKey]);
                    }
                }
            }
        }

        foreach (KeyValuePair<Vector2Int, Crater> kvp in craters)
        {
            SphereCollider sc = GameObjectsPooler.objectPool.GetPooledObject(craterCollider).GetComponent<SphereCollider>();
            
            sc.gameObject.SetActive(true);
            sc.transform.position = new Vector3(kvp.Key.x, 0, kvp.Key.y);
            sc.radius = kvp.Value.radius+2;
            //Debug.Log("SphereCollider " + sc.gameObject.activeSelf + "  " + sc.gameObject.activeInHierarchy + ", " + sc.transform.position + ", " + sc.radius);
        }
    }
}
