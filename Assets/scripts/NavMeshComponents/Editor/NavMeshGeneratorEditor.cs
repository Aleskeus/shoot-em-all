﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(NavMeshGenerator))]
public class NavMeshGeneratorEditor : Editor {

    public override void OnInspectorGUI()
    {
        NavMeshGenerator mapGen = (NavMeshGenerator)target;

        DrawDefaultInspector();


        if (GUILayout.Button("Generate"))
        {
            mapGen.ActivateCollidersFromEditor();
        }
        if (GUILayout.Button("delete"))
        {
            mapGen.DestroyChildSphereColliders();
        }
       
    }
}
